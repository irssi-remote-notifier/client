/*
 * SOFTWARE:
 *   irssi remote notifier
 * 
 * AUTHOR:
 *   Theophile BASTIAN "Tobast" <contact@tobast.fr>
 *
 * WEBSITE:
 *   http://tobast.fr/
 *
 * LICENCE:
 *   GNU GPL v3
 *
 * DESCRIPTION:
 *   A simple SSL-encrypted client/server software to send your irssi notifications from
 *   your IRC bouncer to your desktop.
 *
 * LICENCE HEADER:
 *   Copyright (C) 2013  BASTIAN Theophile
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>.
 */


#ifndef DEF_SOCKETHANDLER
#define DEF_SOCKETHANDLER

#include <QSslSocket>
#include <QString>
#include <QCryptographicHash>
#include <QByteArray>
#include <QList>
#include <QSslError>
#include <QCoreApplication>

#include <iostream>

#include "protocol.h"

class SocketHandler : public QObject
{
	Q_OBJECT
	public:
		struct SocketSettings {
			bool allow_selfsigned;
		};

		SocketHandler(const QString& host, const quint16& port, const QString& pass,
				QObject* parent=0);
		void connect();

		static void setSettings(const SocketSettings& s) { settings = s; }

	private slots:
		void encryptionSet();
		void sockSslErrors(const QList<QSslError>& errors);
		void sockError(QAbstractSocket::SocketError error);
		void sockConnected();
		void sockDisconnected();
		void sockReadyRead();

		void saltReceived(QDataStream& stream);
		void loginAccepted();
		void loginRejected();
		void dataAvailable(QDataStream& stream);

	private:
		QSslSocket sock;
		QString host;
		quint16 port;
		QString pass;
		
		static SocketSettings settings;
};

#endif//DEF_SOCKETHANDLER

