/*
 * SOFTWARE:
 *   irssi remote notifier
 * 
 * AUTHOR:
 *   Theophile BASTIAN "Tobast" <contact@tobast.fr>
 *
 * WEBSITE:
 *   http://tobast.fr/
 *
 * LICENCE:
 *   GNU GPL v3
 *
 * DESCRIPTION:
 *   A simple SSL-encrypted client/server software to send your irssi notifications from
 *   your IRC bouncer to your desktop.
 *
 * LICENCE HEADER:
 *   Copyright (C) 2013  BASTIAN Theophile
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>.
 */


#include "SocketHandler.h"

// Static attibutes declaration
SocketHandler::SocketSettings SocketHandler::settings;

SocketHandler::SocketHandler(const QString& host, const quint16& port, const QString& pass,
				QObject* parent) :
	QObject(parent), sock(this), host(host), port(port), pass(pass)
{}

void SocketHandler::connect()
{
	QObject::connect(&sock, SIGNAL(connected()), this, SLOT(sockConnected()));
	QObject::connect(&sock, SIGNAL(encrypted()), this, SLOT(encryptionSet()));
	QObject::connect(&sock, SIGNAL(sslErrors(const QList<QSslError>&)),
			this, SLOT(sockSslErrors(const QList<QSslError>&)));
	QObject::connect(&sock, SIGNAL(error(QAbstractSocket::SocketError)), 
			this, SLOT(sockError(QAbstractSocket::SocketError)));
	sock.connectToHostEncrypted(host, port);
}

void SocketHandler::encryptionSet()
{
	QObject::connect(&sock, SIGNAL(readyRead()), this, SLOT(sockReadyRead()));

	sock.write(proto::DataPacket(proto::PckRequestSalt)); // Requested salt
}

void SocketHandler::sockSslErrors(const QList<QSslError>& errors)
{
	bool ignore=true;

	for(int err=0; err < errors.size(); err++)
	{
		QSslError::SslError cErr = errors[err].error();
		if(settings.allow_selfsigned && (cErr == QSslError::SelfSignedCertificate
			|| cErr == QSslError::SelfSignedCertificateInChain))
		{
			std::cerr << "Warning: self-signed certificate." << std::endl;
		}
		else
		{
			std::cerr << "SSL error: " << errors[err].errorString().toStdString() << std::endl;
			ignore=false;
		}
	}

	if(ignore)
		sock.ignoreSslErrors();
}

void SocketHandler::sockError(QAbstractSocket::SocketError error)
{
	std::cerr << "Socket error: " << sock.errorString().toStdString() << std::endl;
	if(error == QAbstractSocket::ConnectionRefusedError ||
		error == QAbstractSocket::HostNotFoundError) // ie., the socket will never be connected
	{
		qApp->quit();
	}
}

void SocketHandler::sockConnected()
{
	QObject::connect(&sock, SIGNAL(disconnected()), this, SLOT(sockDisconnected()));
}
void SocketHandler::sockDisconnected()
{
	std::cerr << "Disconnected from server." << std::endl;
	qApp->quit();
}

void SocketHandler::sockReadyRead()
{
	QDataStream stream(&sock);
	while(sock.bytesAvailable() > 0) {
		quint16 packetType;
		stream >> packetType;

		switch(packetType)
		{
			case proto::PckSendSalt:
				saltReceived(stream);
				break;
			case proto::PckLoginAccepted:
				loginAccepted();
				break;
			case proto::PckLoginRejected:
				loginRejected();
				break;
			case proto::PckDataAvailable:
				dataAvailable(stream);
				break;
			default:
				sock.readAll(); // undefined packet ; purge input.
				break;
		}
	}
}

void SocketHandler::saltReceived(QDataStream& stream)
{
	QByteArray salt;
	proto::DataPacket::getByteArray(salt, stream);

	QCryptographicHash hasher(proto::HASH_ALGORITHM);
	hasher.addData(pass.toUtf8());
	hasher.addData(salt);
	
	sock.write(proto::DataPacket::make(proto::PckRequestLogin, hasher.result()));
}

void SocketHandler::loginAccepted()
{
	std::cout << "Logged in, now receiving transmissions." << std::endl;
}

void SocketHandler::loginRejected()
{
	std::cerr << "Login rejected: wrong password." << std::endl;
	qApp->quit();
}

void SocketHandler::dataAvailable(QDataStream& stream)
{
	QByteArray data;
	proto::DataPacket::getByteArray(data, stream);

	std::cout << QString(data).toStdString() << std::endl;
}

