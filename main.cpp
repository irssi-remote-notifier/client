
/*
 * SOFTWARE:
 *   irssi remote notifier
 * 
 * AUTHOR:
 *   Theophile BASTIAN "Tobast" <contact@tobast.fr>
 *
 * WEBSITE:
 *   http://tobast.fr/
 *
 * LICENCE:
 *   GNU GPL v3
 *
 * DESCRIPTION:
 *   A simple SSL-encrypted client/server software to send your irssi notifications from
 *   your IRC bouncer to your desktop.
 *
 * LICENCE HEADER:
 *   Copyright (C) 2013  BASTIAN Theophile
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>.
 */

#include <QCoreApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QLocale>

#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>

#include "ClientWorker.h"
#include "SocketHandler.h"

void print_help(const char* selfPath)
{
	std::cerr << "Error: missing or invalid arguments. Usage:\n"
		<< selfPath << " [options] server_address server_port password\n"
		<< "Please note that the order is _NOT_ optionnal.\n\n"
		<< "Options:\n"
		<< "--------\n\n"
		<< "--self-signed\t\tThe connection will not drop if the server uses "
		<< "a self-signed certificate.\n\n"
		<< "Please refer to the manpage for more details.\n";
}

int main(int argc, char** argv)
{
	// Read connection details from argv
	if(argc < 4) // Not enough arguments
	{
		print_help(argv[0]);
		return 1;
	}
	std::string addr(argv[argc-3]), pass(argv[argc-1]);
	unsigned int port = atoi(argv[argc-2]);
	if(port == 0)
	{
		print_help(argv[0]);
		return 1;
	}


	QCoreApplication a(argc, argv);

	// Setup translator
	QString locale = QLocale::system().name().section('_', 0, 0);
	QTranslator translator;
	translator.load(QString("qt_") + locale,
			QLibraryInfo::location(QLibraryInfo::TranslationsPath));
	a.installTranslator(&translator);

	// Socket settings
	SocketHandler::SocketSettings sockSetts;
	sockSetts.allow_selfsigned=false;
	for(int arg=1; arg<argc; arg++)
	{
		if(strcmp(argv[arg],"--self-signed") == 0)
			sockSetts.allow_selfsigned=true;
	}

	SocketHandler::setSettings(sockSetts);


	ClientWorker worker(addr.c_str(), port, pass.c_str());
	worker.exec();

	return a.exec();
}

